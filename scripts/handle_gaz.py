import pandas as pd
import numpy as np

path_to_gaz = './data/raw/gaz.csv'

df = pd.read_csv(path_to_gaz, parse_dates=['date'], infer_datetime_format=True)

df['duration'] = df['date'].diff()/np.timedelta64(1, 'm')
df['middle'] = df['date'] - df['date'].diff()/2
df['gaz_volume_per_min'] = df['gaz_volume'].diff() / df['duration']

df.to_csv('./data/gaz.csv', index=False)