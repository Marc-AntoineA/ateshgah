from gazpar_client import GazparClient
from influxdb_data_interface import InfuxdbDataInterface

import yaml
import os

if __name__ == '__main__':
    
    with open(os.path.join(os.path.dirname(__file__), '../config.yml'), 'r') as f:
        config = yaml.safe_load(f)

    influxdb_data_interface = InfuxdbDataInterface(
        config['influxdb']['organization'],
        config['influxdb']['bucket'],
        config['influxdb']['api_token'],
        config['influxdb']['host']
    )

    print('> Requesting data from Gazpar')
    gazpar_config = config['gazpar']
    for gazpar in gazpar_config:
        gazpar_client = GazparClient(gazpar['username'], gazpar['password'],
            gazpar['pce'], influxdb_data_interface, label=gazpar['label'])

        gazpar_client.download_new_data()
