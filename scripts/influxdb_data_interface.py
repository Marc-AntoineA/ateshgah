from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

from typing import Dict

class InfuxdbDataInterface:

    def __init__(self, org: str, bucket: str, token: str, host: str):
        self._org = org
        self._bucket = bucket
        self._token = token
        self._host = host

    def _client(self):
        return InfluxDBClient(url=self._host, token=self._token, org=self._org)

    def query(self, query_str: str):
        with self._client() as client:
            query_api = client.query_api()
            result = query_api.query(org=self._org, query=query_str)
            return result

    def last_entry_date(self, measurement, tags: Dict[str, str]):
        filters_lines = ''
        for tag, value in tags.items():
            filters_lines += f' and \n r.{ tag } == "{ value }"'

        query = f'''
from(bucket: "{ self._bucket }")
|> range(start: -3y)
|> filter(fn: (r) => r._measurement == "{ measurement }" { filters_lines })
|> last()
        '''
        result = self.query(query_str=query)

        for table in result:
            for record in table.records:
                return record.get_time()
        return None

    def save_data(self, measurement, tags, values):

        with self._client() as client:
            write_api = client.write_api(write_options=SYNCHRONOUS)

            for time, value in values.items():
                point = Point(measurement)\
                    .time(time)

                for tag, tag_value in tags.items():
                    point = point.tag(tag, value=tag_value)

                for field, field_value in value.items():
                    point = point.field(field, value=field_value)
                write_api.write(self._bucket, self._org, point)
