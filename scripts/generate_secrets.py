import secrets
import string

alphabet = string.ascii_letters + string.digits

grafana_password = ''.join(secrets.choice(alphabet) for i in range(10))
influxdb_password = ''.join(secrets.choice(alphabet) for i in range(10))
ateshgah_app_password = ''.join(secrets.choice(alphabet) for i in range(10))

secret_key = secrets.token_urlsafe(128)

print('Update your .env with the following values')

print(f'INFLUXDB_ADMIN_TOKEN="{ secret_key }"')
print(f'INFLUXDB_PASSWORD="{ influxdb_password }"')
print(f'GRAFANA_PASSWORD="{ grafana_password }"')

print(f'ATESHGAH_APP_PASSWORD="{ ateshgah_app_password }"')