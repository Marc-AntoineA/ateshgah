import pygazpar
import datetime
import pytz

class GazparClient:

    def __init__(self, username, password, pce, data_interface, label):

        self._pygazparClient = pygazpar.Client(pygazpar.JsonWebDataSource(
            username=username,
            password=password
        ))
        self._pce = pce
        self._data_interface = data_interface
        self._label = label

    def _request_since(self, nb_days):
        '''
        Max 1095 days
        '''
        try:
            data = self._pygazparClient.loadSince(pceIdentifier=self._pce,
                            lastNDays=nb_days,
                            frequencies=[pygazpar.Frequency.DAILY])['daily']

            for k, elt in enumerate(data):
                data[k]['time_period'] = datetime.datetime.strptime(elt['time_period'], "%d/%m/%Y").isoformat()

            return data

        except Exception as e:
            print(e)
            return None

    def download_new_data(self):

        def _float_or_none(value):
            if value is None:
                return None
            return float(value)

        last_entry_date = self._data_interface.last_entry_date('energy', { 'label': self._label, 'source': 'gazpar' })
        if last_entry_date is None:
            nb_days_to_request = 1095
        else:
            today = datetime.datetime.now(pytz.utc)
            nb_days_to_request = (today - last_entry_date).days + 1

        data = self._request_since(nb_days_to_request)

        data_dict = {
            row['time_period']: {
                'volume_m3': _float_or_none(row['volume_m3']),
                'energy_kwh': _float_or_none(row['energy_kwh']),
                'converter_factor_kwh/m3': _float_or_none(row['converter_factor_kwh/m3']),
                'temperature_degC': _float_or_none(row['temperature_degC']),
                'type': row['type'],
            } for row in data
        }
        self._data_interface.save_data('energy',
            { 'label': self._label, 'source': 'gazpar', 'pce': self._pce }, data_dict)