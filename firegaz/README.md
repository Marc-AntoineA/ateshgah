# Firegaz

FireGaz est une extension Firefox pour récupérer les données depuis son espace client GRDF et exporter les données sur la base de données InfluxDB proposée par Ateshgah.

Cela fonctionne également avec son compte [metroenergies.fr](https://metroenergies.fr/)

## Pré-requis

* Créer un fichier `src/secrets.js` avec ses données de connexion `InfluxDB` et la liste des PCE utilisés.
```js
const secrets = {
    "INFLUXDB": {
        "URL":"<>",
        "DB_TOKEN":"<>",
        "ORG": "ateshgah",
        "BUCKET": "ateshgah"
    },
    "labels": {
        "<>": "mon_label_pce_1",
        "<>": "mon_label_pce_2",
    },
    "metroenergies_label": "<>",
}

export default secrets;
```

* L’extension Firefox n’est pour le moment pas signée (à voir pour plus tard?). Une astuce est donc d’utiliser [Firefox Developer Edition](https://www.mozilla.org/fr/firefox/developer/)


_Ptet des dépendances npm globales ?_

### Développement

* Installer les dépendances et compiler tout en un dossier `dist`
```
npm install
npm start
```

* Dans un autre terminal, après s’être déplacé dans le dossier `dist` :  installer l’extension Firefox dans un environnement temporaire de développement

```
web-ext run --devtools --verbose
```

### Production

* Installer les dépendances
```
npm install
npm build
```

* Créer un fichier `.xps` qui permet d’installer l’extension sur Firefox

```
web-ext build
```

## Utilisation

1. Se rendre sur son espace client `monespace.grdf.fr`
2. Se connecter
3. Les données des 30 derniers jours sont automatiquement ajoutées sur sa base de données InfluxDb

Une bordure de couleur apparaît tout autour de la page :
* `rouge` en cas d’erreur ;
* `orange` en cours d’exécution ;
* `vert` si les données ont bient été ajoutées sur InfluxDB

## Crédits

Favicon generated with https://favicon.io/favicon-generator/