import { InfluxDB, Point, HttpError, consoleLogger } from '@influxdata/influxdb-client-browser'
import secrets from './secrets.js'

function setStatus(color) {

    document.body.style.border = `5px solid ${color}`;
}

function requestGrdfData(begin, end, pce) {
    const dataUrl = `https://monespace.grdf.fr/api/e-conso/pce/consommation/informatives?dateDebut=${begin}&dateFin=${end}&pceList%5B%5D=${pce}`;

    return new Promise((resolve, reject) => {
        fetch(dataUrl)
        .then(function(response) {
            response.json().then((data) => {
                resolve(data);
            });
        })
        .catch((err) => {
            reject(err);
        })
    });
}

function requestGrdfTemperature(end, nbDays, pce) {
    const temperatureUrl = `https://monespace.grdf.fr/api/e-conso/pce/${pce}/meteo?dateFinPeriode=${end}&nbJours=${nbDays}`

    return new Promise((resolve, reject) => {
        fetch(temperatureUrl)
        .then(function(response) {
            response.json().then((data) => {
                resolve(data);
            });
        })
        .catch((err) => {
            reject(err);
        })
    });
}

function requestMetroenergiesData(beginDate, endDate) {

    const requestUrl = `https://metroenergies.fr/S2G-MT-Usager-Back/rest/mt/usager/conso/daily?startDate=${ beginDate.toGMTString() }&endDate=${ endDate.toGMTString() }&version=0;`

    const token = localStorage.getItem('JWT-CITIZEN-PORTAL');

    return new Promise((resolve, reject) => {
    fetch(requestUrl, { headers: { Authorization: token } })
        .then(function(response) {
            response.json().then((data) => {
                resolve(data);
            });
        })
        .catch((err) => {
            reject(err);
        })
    });
}

function requestMetroEnergiesTemperature(beginDate, endDate) {
    const requestUrl = `https://metroenergies.fr/S2G-MT-Usager-Back/rest/mt/usager/weather?endDate=${ endDate.toGMTString() }&scale=daily&startDate=${ beginDate.toGMTString() }`

    const token = localStorage.getItem('JWT-CITIZEN-PORTAL');

    return new Promise((resolve, reject) => {
    fetch(requestUrl, { headers: { Authorization: token } })
        .then(function(response) {
            response.json().then((data) => {
                resolve(data['response']);
            });
        })
        .catch((err) => {
            reject(err);
        })
    });

}

function scrapePCE() {
    var strongTags = document.getElementsByTagName("strong");
    var searchText = 'N° PCE';
    console.log(strongTags)
    for (const strong of strongTags) {
        if (strong.textContent == searchText) {
            const pce_str = strong.parentElement.childNodes[1].textContent
            return pce_str.replaceAll(' ', '')
        }
    }
    return undefined
}

function getWriteApi() {
    const influxdb_conf = secrets["INFLUXDB"];
    const writeApi = new InfluxDB({
        url: influxdb_conf["URL"],
        token: influxdb_conf["DB_TOKEN"]
    }).getWriteApi(influxdb_conf['ORG'], influxdb_conf['BUCKET'], 'ns')

    return writeApi;
}

function closeWriteApi(writeApi) {
    try {
        writeApi.close().then(() => {
            setStatus('green')
        }).catch((e) => {
            setStatus('red')
            console.error(e)
        })
    } catch (e) {
        setStatus('red')
        if (e instanceof HttpError && e.statusCode === 401) {
            console.log('Run ./onboarding.js to setup a new InfluxDB database.')
        }
        console.log('\nFinished ERROR')
    }
}

function main() {
    if (window.location.hostname === 'login.monespace.grdf.fr') {
        // document.getElementById('mail').value = secrets['username']
        // document.getElementById('pass').value = secrets['password']

    } else if (window.location.hostname === 'monespace.grdf.fr') {
        setStatus('orange')
        const pce = scrapePCE();

        if (pce === undefined) {
            console.error('PCE not found')
            setStatus('red')
            return;
        }

        const label = secrets['labels'][pce];

        const today = new Date();

        const yesterday = new Date(); yesterday.setDate(yesterday.getDate() - 1);
        const first_day_of_range = new Date(); first_day_of_range.setDate(first_day_of_range.getDate() - 30);

        const today_str = today.toISOString().slice(0,10)
        const start_range_str = first_day_of_range.toISOString().slice(0,10)
        const yesterday_str = yesterday.toISOString().slice(0,10)

        const promises = [];
        promises.push(requestGrdfData(start_range_str, today_str, pce));
        promises.push(requestGrdfTemperature(yesterday_str, "59", pce));

        Promise.all(promises).then((values) => {
            const [data_, temperatures] = values;

            const writeApi = getWriteApi();
            const records = data_[pce]["releves"];

            for (const record of records) {
                const d_str = record['journeeGaziere'];
                const d = new Date(d_str);

                let point1 = new Point('energy')
                    .tag('label', label)
                    .tag('source', secrets['source'])
                    .timestamp(d);

                if (record['volumeBrutConsomme'] !== null)
                    point1 = point1.floatField('volume_m3', record['volumeBrutConsomme']);

                if (record['energieConsomme'] !== null)
                    point1 = point1.floatField('energy_kwh', record['energieConsomme']);

                if (record['coeffConversion'] !== null)
                    point1 = point1.floatField('conversion_factor', record['coeffConversion']);

                if (record['qualificationReleve'] !== null)
                    point1 = point1.stringField('type', record['qualificationReleve']);

                if (temperatures[d_str])
                    point1 = point1.floatField('temperature_degC', temperatures[d_str]);

                writeApi.writePoint(point1);
            }

            closeWriteApi(writeApi);
        });

    } else if (window.location.hostname === 'metroenergies.fr') {
        setStatus('orange');

        const today = new Date();

        const first_day_of_range = new Date(); first_day_of_range.setDate(first_day_of_range.getDate() - 200);

        const promises = [];
        promises.push(requestMetroenergiesData(first_day_of_range, today));
        promises.push(requestMetroEnergiesTemperature(first_day_of_range, today));

        Promise.all(promises).then((values) => {
            const [data, temperatures] = values;
            const temperatures_degC = temperatures.reduce((a, v) => ({ ...a, [v['WDate']]: v['tempAverage'] - 273.15}), {})

            const elec = data['object']['elec']['individuel']['consumptions']
            const gaz = data['object']['gas']['individuel']['consumptions']

            const writeApi = getWriteApi();

            for (const record of elec) {
                const d = new Date(record['date']);

                let point1 = new Point('energy')
                    .tag('label', secrets['metroenergies_label'])
                    .tag('source', 'metroenergy')
                    .tag('energy', 'elec')
                    .timestamp(d)
                    .floatField('energy_kwh', record['energy'])
                    .floatField('cost_euro', record['cost']);

                if (temperatures_degC[record['date']])
                    point1 = point1.floatField('temperature_degC', temperatures_degC[record['date']]);

                writeApi.writePoint(point1);
            }

            for (const record of gaz) {
                const d = new Date(record['date']);

                let point1 = new Point('energy')
                    .tag('label', secrets['metroenergies_label'])
                    .tag('source', 'metroenergy')
                    .tag('energy', 'gaz')
                    .timestamp(d)
                    .floatField('energy_kwh', record['energy'])
                    .floatField('cost_euro', record['cost']);

                writeApi.writePoint(point1);
            }

            closeWriteApi(writeApi);
        });
    }
}

setTimeout(main, 500);