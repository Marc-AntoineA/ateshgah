# Ateshgah App

## Paramètres

Fichier `assets/config.json`

```json
{
    "INFLUXDB": {
        "URL":"...",
        "DB_TOKEN":"...",
        "ORG": "ateshgah",
        "BUCKET": "ateshgah"
    }
}
```

Fichier `assets/influxdb_schema.json`
```json
[
    {
        "id": "gaz_xxx",
        "label": "Gaz xxx",
        "location": "xxx",
        "unit": "m3",
        "cumulative": "true",
        "measurement": "energy"
    },
    ...
]
```
