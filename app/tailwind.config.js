/** @type {import('tailwindcss').Config} */
module.exports = {
 content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
      'orange': {
        DEFAULT: '#FF6201',
        '50': '#FFD3B9',
        '100': '#FFC7A4',
        '200': '#FFAE7B',
        '300': '#FF9453',
        '400': '#FF7B2A',
        '500': '#FF6201',
        '600': '#C84C00',
        '700': '#903700',
        '800': '#582100',
        '900': '#200C00'
       }
      },
    }
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
