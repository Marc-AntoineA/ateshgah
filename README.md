# Ateshgah

> . « Atechgah » signifie « maison du feu », « lieu du feu ». Le territoire du temple regorge de gaz naturel ce qui déclenche parfois des phénomènes naturels uniques. 
>
> [Wikipedia](https://fr.wikipedia.org/wiki/Atechgah)

> Le feu éternel de l'ateshgah s'est éteint en 1969. Après un siècle d'exploitation intense, le réservoir d'ydrocarbures dont il s'échappait s'est tari. 
>
> « Or Noir, la Grande histoire du pétrole », Matthieu Auzanneau

_Ateshgah_ est un logiciel pour suivre sa consommation de gaz naturel, _tant qu’il en reste_. Son usage est à destination d’un public français, toute la documentation est donc en français.

Le logiciel est basé sur Grafana pour la visualisation et InfluxDb pour le stockage des données.

Les données du compteur de gaz connecté Gazpar sont obtenues grâce grâce au module Python [Pygazpar](https://github.com/ssenart/PyGazpar).

**Todo** : données linky

## Installation

Ce repo propose un container docker, très rapide à mettre en place.

### 1. Récupérer le code source

Récupérer le code source sur son ordinateur
```
git clone https://framagit.org/Marc-AntoineA/ateshgah.git
```

### 2. Variables d’environnement

Créer un fichier `.env` à partir du fichier `.env.example`
```
cp .env.example .env
```

Pour créer les mots de passe, il est possible d’utiliser le script `scripts/generate_secrets.py`

```
python3 scripts/generate_secrets.py
```



## Mise en place et données

### Relevés manuels de gaz

Créer un fichier `data/raw/gaz.csv` (`,` comme **séparateur**)contenant les deux colonnes suivantes.


| Colonne | Type de données   | Exemple |
|---------|-------------------|---------|
| date    | YYYYMMDDTHH:MM:SS | `20221211T11:52:00` |
| gaz_volume | Nombre flottant, en m³ | 50.43 |

Le script `scripts/handle_gaz.py` permet de convertir ce fichier dans le fichier suivant `data/gaz.csv` avec davantage de colonnes.

### Récupération des données Gazpar

**Prérequis** : Disposer d’un compteur communiquant Gazpar.

**Todo** : Code pour créer un dashboard ? & toutes les tâches ?